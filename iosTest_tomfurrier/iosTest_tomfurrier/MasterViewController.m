//
//  MasterViewController.m
//  iosTest_tomfurrier
//
//  Created by Dev on 3/7/14.
//  Copyright (c) 2014 Dev. All rights reserved.
//

#define bgQueue dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)
#define goAppStreamURL [NSURL URLWithString:@"http://www.goappstream.com/api/v1/app_deals.json"]


#import "MasterViewController.h"

#import "DetailViewController.h"
#import "AppDealModel.h"
#import "AppDealCell.h"
#import "Util.h"

@interface MasterViewController () {
    NSMutableArray *_objects;
}
@end

@implementation MasterViewController

@synthesize appDeals = _appDeals;

- (void)awakeFromNib
{
    [super awakeFromNib];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
//    self.navigationItem.leftBarButtonItem = self.editButtonItem;

//    UIBarButtonItem *addButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(insertNewObject:)];
//    self.navigationItem.rightBarButtonItem = addButton;
    self.title = @"App Deals";
    
    // last refreshed set to 2001
    _lastRefreshDate = [[NSDate alloc] initWithTimeIntervalSinceReferenceDate:0];
    _appDeals = [[NSMutableArray alloc] init];
    _documentsDirectoryPath = [Util applicationDocumentsDirectory];
    
    // load data
    dispatch_async(bgQueue, ^{
        NSData* data = [NSData dataWithContentsOfURL:
                        goAppStreamURL];
        
        [self fetchedData:data];
    });
}

- (void)fetchedData:(NSData *)responseData {
    //parse out the json data
    NSError *error;
    NSDictionary *json = [NSJSONSerialization
                          JSONObjectWithData:responseData
                          
                          options:kNilOptions
                          error:&error];
    
    NSDictionary *deals = [json objectForKey:@"mac_app_deals"];
    NSString *dataUrl = [deals objectForKey:@"data_url"];
    
    NSDateFormatter *inputFormatter = [[NSDateFormatter alloc] init];
    [inputFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZ"];
    NSDate *refreshedAt = [inputFormatter dateFromString:[deals objectForKey:@"refreshed_at"]];
    
//    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
//    NSDateComponents *offsetComponents = [[NSDateComponents alloc] init];
//    [offsetComponents setMinute:5];
//    NSDate *lastRefreshWithOffset = [gregorian dateByAddingComponents:offsetComponents toDate:_lastRefreshDate options:0];
    
    // we only refresh when the current information is older than 5 minutes
//    if (refreshedAt > lastRefreshWithOffset ) {
        _lastRefreshDate = refreshedAt;
        DLog(@"Refresing _lastRefreshDate= %@", [inputFormatter stringFromDate:refreshedAt]);
        
        json = [NSJSONSerialization
                JSONObjectWithData:[NSData dataWithContentsOfURL:
                                                   [NSURL URLWithString:dataUrl]]
                 
                 options:kNilOptions
                 error:&error];
        
        NSArray *apps = [json objectForKey:@"apps"];
        
        for (NSDictionary *app in apps) {
            
            NSString *title = [app objectForKey:@"title"];
            NSNumber *price = [app objectForKey:@"current_price"];
            NSString *currency = [app objectForKey:@"currency_code"];
            NSString *iconURL = [app objectForKey:@"icon"];
            NSNumber *appId = [app objectForKey:@"id"];
            
            [_appDeals addObject:[[AppDealModel alloc] initWithTitle:title currency:currency price:[price floatValue] iconURL:iconURL appId:[appId integerValue]] ];
        }
    
    dispatch_async(dispatch_get_main_queue(),
                   ^{
                              [self.tableView reloadData]; 
                   });
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)insertNewObject:(id)sender
{
    if (!_objects) {
        _objects = [[NSMutableArray alloc] init];
    }
    [_objects insertObject:[NSDate date] atIndex:0];
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    [self.tableView insertRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
}

#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _appDeals.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    __block AppDealCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MyCustomCell" forIndexPath:indexPath];

    AppDealModel *deal = [self.appDeals objectAtIndex:indexPath.row];
    cell.titleLabel.text = deal.title;
    cell.priceLabel.text = [NSString stringWithFormat:@"%.02f",deal.price];
    cell.currencyLabel.text = deal.currency;
    
    dispatch_async(bgQueue, ^{
        __block UIImage *icon = [Util getCachedImage:[NSString stringWithFormat:@"%d",deal.appId] OrDownloadFromURL:deal.iconURL];
        
        dispatch_sync(dispatch_get_main_queue(), ^{
            cell.iconImageView.image = icon;
            [cell setNeedsLayout];
        });
    });
    
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return NO;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        [_objects removeObjectAtIndex:indexPath.row];
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
    }
}

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"showDetail"]) {
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        NSDate *object = _objects[indexPath.row];
        [[segue destinationViewController] setDetailItem:object];
    }
}

@end
