//
//  AppDealModel.h
//  iosTest_tomfurrier
//
//  Created by Dev on 3/7/14.
//  Copyright (c) 2014 Dev. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AppDealModel : NSObject

@property (strong) UIImage      *icon;
@property (strong) NSString     *title;
@property (strong) NSString     *currency;
@property (assign) float         price;
@property (strong) NSString     *iconURL;
@property (assign) int           appId;

- (id)initWithTitle:(NSString*)title
           currency:(NSString*)currency
              price:(float)price
            iconURL:(NSString*)iconURL
              appId:(int)appId;

- (id)initWithTitle:(NSString*)title
           currency:(NSString*)currency
              price:(float)price
               icon:(UIImage*)icon
              appId:(int)appId;


@end
