//
//  AppDelegate.h
//  iosTest_tomfurrier
//
//  Created by Dev on 3/7/14.
//  Copyright (c) 2014 Dev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
