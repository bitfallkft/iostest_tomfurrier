//
//  AppDealCell.m
//  iosTest_tomfurrier
//
//  Created by Dev on 3/7/14.
//  Copyright (c) 2014 Dev. All rights reserved.
//

#import "AppDealCell.h"

@implementation AppDealCell

@synthesize titleLabel;
@synthesize priceLabel;
@synthesize currencyLabel;
@synthesize iconImageView;

- (void)prepareForReuse {
    [self.iconImageView setImage:[UIImage new]];
}

@end
