//
//  MasterViewController.h
//  iosTest_tomfurrier
//
//  Created by Dev on 3/7/14.
//  Copyright (c) 2014 Dev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MasterViewController : UITableViewController

@property (strong) NSDate               *lastRefreshDate;
@property (strong) NSMutableArray       *appDeals;

@property (strong, nonatomic) NSString  *documentsDirectoryPath;

@end
