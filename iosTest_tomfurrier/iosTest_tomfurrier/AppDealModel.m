//
//  AppDealModel.m
//  iosTest_tomfurrier
//
//  Created by Dev on 3/7/14.
//  Copyright (c) 2014 Dev. All rights reserved.
//

#import "AppDealModel.h"

@implementation AppDealModel

@synthesize title = _title;
@synthesize currency = _currency;
@synthesize price = _price;
@synthesize icon = _icon;
@synthesize iconURL = _iconURL;
@synthesize appId = _appId;

- (id)initWithTitle:(NSString*)title
           currency:(NSString*)currency
              price:(float)price
               icon:(UIImage *)icon
              appId:(int)appId{
    if((self = [super init])){
        self.title = title;
        self.currency = currency;
        self.price = price;
        self.icon = icon;
        self.appId = appId;
    }
    return self;
}

- (id)initWithTitle:(NSString*)title
           currency:(NSString*)currency
              price:(float)price
            iconURL:(NSString*)iconURL
              appId:(int)appId{
    if((self = [super init])){
        self.title = title;
        self.currency = currency;
        self.price = price;
        self.iconURL = iconURL;
        self.appId = appId;
    }
    return self;
}
@end
