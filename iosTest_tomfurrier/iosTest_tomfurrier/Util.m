//
//  Util.m
//  iosTest_tomfurrier
//
//  Created by Dev on 3/10/14.
//  Copyright (c) 2014 Dev. All rights reserved.
//

#import "Util.h"

@implementation Util

static NSString * documentsDirectoryPath = nil;

+ (NSString *) applicationDocumentsDirectory
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
    return basePath;
}


+ (void)saveImage:(UIImage *)image withName:(NSString *)name inDirectory:(NSString *)path{
    NSData *data = UIImageJPEGRepresentation(image, 1.0);
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *fullPath = [path stringByAppendingPathComponent:name];
    [fileManager createFileAtPath:fullPath contents:data attributes:nil];
}


+ (UIImage *)loadImage:(NSString *)name fromDirectory:(NSString *)path{
    NSString *fullPath = [path stringByAppendingPathComponent:name];
    UIImage *img = [UIImage imageWithContentsOfFile:fullPath];
    
    return img;
}


+ (UIImage *) getImageFromURL:(NSString *)fileURL {
    UIImage * result;
    
    NSData * data = [NSData dataWithContentsOfURL:[NSURL URLWithString:fileURL]];
    result = [UIImage imageWithData:data];
    
    return result;
}


+ (UIImage *) getCachedImage:(NSString *)imageName OrDownloadFromURL:(NSString *) url toDirectory:(NSString *) path{
    
    UIImage * result;
    
    NSString* fullPath = [path stringByAppendingPathComponent:imageName];
    BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:fullPath];
    
    if (fileExists == YES) {
        DLog(@"Image named: %@ loaded from disk. fullpath: %@", imageName, fullPath);
        result = [UIImage imageWithContentsOfFile:fullPath];
    } else {
        DLog(@"Image named: %@ downloaded from url: %@", imageName, url);
        NSData * data = [NSData dataWithContentsOfURL:[NSURL URLWithString:url]];
        result = [UIImage imageWithData:data];
        [Util saveImage:result withName:imageName inDirectory:path];
    }
    
    return result;
}


+ (UIImage *) getCachedImage:(NSString *)imageName OrDownloadFromURL:(NSString *) url {
    
    if ( documentsDirectoryPath == nil ) {
        documentsDirectoryPath = [Util applicationDocumentsDirectory];
    }
    
    return [Util getCachedImage:imageName OrDownloadFromURL:url toDirectory:documentsDirectoryPath];
}

@end
