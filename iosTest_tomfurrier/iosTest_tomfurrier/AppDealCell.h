//
//  AppDealCell.h
//  iosTest_tomfurrier
//
//  Created by Dev on 3/7/14.
//  Copyright (c) 2014 Dev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDealCell : UITableViewCell

@property (nonatomic, strong) IBOutlet UILabel *titleLabel;
@property (nonatomic, strong) IBOutlet UILabel *priceLabel;
@property (nonatomic, strong) IBOutlet UILabel *currencyLabel;
@property (nonatomic, strong) IBOutlet UIImageView *iconImageView;

@end
