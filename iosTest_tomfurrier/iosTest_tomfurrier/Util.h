//
//  Util.h
//  iosTest_tomfurrier
//
//  Created by Dev on 3/10/14.
//  Copyright (c) 2014 Dev. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Util : NSObject

// returns the Documents directory path
+ (NSString *)  applicationDocumentsDirectory;

+ (void)        saveImage:(UIImage *)image withName:(NSString *)name inDirectory:(NSString *)path;

+ (UIImage *)   loadImage:(NSString *)name fromDirectory:(NSString *)path;

+ (UIImage *)   getImageFromURL:(NSString *)fileURL;

+ (UIImage *)   getCachedImage:(NSString *)imageName OrDownloadFromURL:(NSString *) url toDirectory:(NSString *) path;
+ (UIImage *)   getCachedImage:(NSString *)imageName OrDownloadFromURL:(NSString *) url;

@end
